# DORA Metrics Demo - Static

Currently the best way to present this topic is by using the DORA metrics from GitLab.com. Here is a great intro video: https://youtu.be/1BrcMV6rCDw that gives a good base on what you should talk about. This is a great ultimate seller as almost everything exists within the ultimate tier.

The value stream dashboard is here: https://gitlab.com/gitlab-org/gitlab/-/value_stream_analytics?created_after=2022-11-02&created_before=2022-12-01&stage_id=issue&sort=end_event&direction=desc&page=1

Doc on what DORA is: https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance

Key points:

- A higher deployment frequency means you can get feedback sooner and iterate faster to deliver improvements and features.
- Over time, the lead time for changes should decrease, while your team’s performance should increase.
